package com.example.khaicoder.retrofit_rxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.example.khaicoder.retrofit_rxjava.adapter.Adapter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity implements Adapter.IGetItem {
    private RecyclerView recyclerView;
    private Adapter adapter;
    private IMyApi iMyApi;
    private CompositeDisposable disposable = new CompositeDisposable();
    private List<Model> m = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Retrofit retrofitClient = RetrofitClient.getRetrofit();
        iMyApi = retrofitClient.create(IMyApi.class);
        adapter = new Adapter(this);
        init();

    }

    private void init() {
        disposable.add(iMyApi.getModel("xa em", "sdsd")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Model>>() {
                    @Override
                    public void accept(List<Model> models) throws Exception {
                        m = models;
                        initRecy();
                    }
                }));
    }

    private void initRecy() {
        recyclerView = findViewById(R.id.rc_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public int getCount() {
        if (m == null){
            return 0;
        }
            return m.size();
    }

    @Override
    public Model getItems(int position) {
        return m.get(position);
    }
}
