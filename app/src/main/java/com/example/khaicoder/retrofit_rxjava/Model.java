package com.example.khaicoder.retrofit_rxjava;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Model {


    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Artist")
    @Expose
    private String artist;
    @SerializedName("Avatar")
    @Expose
    private String avatar;
    @SerializedName("UrlJunDownload")
    @Expose
    private String urlJunDownload;
    @SerializedName("LyricsUrl")
    @Expose
    private String lyricsUrl;
    @SerializedName("UrlSource")
    @Expose
    private String urlSource;
    @SerializedName("SiteId")
    @Expose
    private String siteId;
    @SerializedName("HostName")
    @Expose
    private String hostName;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUrlJunDownload() {
        return urlJunDownload;
    }

    public void setUrlJunDownload(String urlJunDownload) {
        this.urlJunDownload = urlJunDownload;
    }

    public String getLyricsUrl() {
        return lyricsUrl;
    }

    public void setLyricsUrl(String lyricsUrl) {
        this.lyricsUrl = lyricsUrl;
    }

    public String getUrlSource() {
        return urlSource;
    }

    public void setUrlSource(String urlSource) {
        this.urlSource = urlSource;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
}
