package com.example.khaicoder.retrofit_rxjava.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.khaicoder.retrofit_rxjava.Model;
import com.example.khaicoder.retrofit_rxjava.R;
import com.squareup.picasso.Picasso;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    private IGetItem iGet;

    public Adapter(IGetItem iGet) {
        this.iGet = iGet;
    }

    private View view;
    @NonNull
    @Override
    public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ViewHolder holder, int position) {
        Model model = iGet.getItems(position);
        Picasso.with(view.getContext()).load(model.getAvatar()).into(holder.img);
        holder.tvName.setText(model.getTitle());

    }

    @Override
    public int getItemCount() {
        return iGet.getCount();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvName;
        private ImageView img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_name);
            img = itemView.findViewById(R.id.img);
        }
    }

    public interface IGetItem {
        int getCount();

        Model getItems(int position);
    }
}
