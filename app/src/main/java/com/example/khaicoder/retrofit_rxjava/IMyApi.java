package com.example.khaicoder.retrofit_rxjava;


import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface IMyApi {
    //    http://j.ginggong.com/jOut.ashx?k=em&h=mp3.zing.vn&code=[mã được cung cấp]
    @GET("jOut.ashx")
    Observable<List<Model>> getModel(
            @Query("k") String title,
            @Query("code") String code
    );

}
